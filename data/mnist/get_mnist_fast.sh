#!/usr/bin/env sh
# This scripts downloads the mnist data and unzips it.

DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd "$DIR"

echo "Downloading..."

wget https://gitee.com/aczz/mnist/repository/archive/master.zip
unzip master.zip

for fname in train-images-idx3-ubyte train-labels-idx1-ubyte t10k-images-idx3-ubyte t10k-labels-idx1-ubyte
do
    gunzip mnist/${fname}.gz
    mv mnist/${fname} ./
done
