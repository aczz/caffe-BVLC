#!/usr/bin/env sh
#
# N.B. This does not download the ilsvrcC12 data set, as it is gargantuan.
# This script downloads the imagenet example auxiliary files including:
# - the ilsvrc12 image mean, binaryproto
# - synset ids and words
# - Python pickle-format data of ImageNet graph structure and relative infogain
# - the training splits with labels

DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd "$DIR"

fname=caffe_ilsvrc12.tar.gz
if [ ! -e $fname ]; then
    echo "Downloading..."

    #wget -c http://dl.caffe.berkeleyvision.org/caffe_ilsvrc12.tar.gz
    wget -c https://gitee.com/aczz/caffe_ilsvrc12/repository/archive/master.zip
    unzip master.zip
    mv caffe_ilsvrc12/caffe_ilsvrc12.tar.gz ./
fi

echo "Unzipping..."

tar -xf caffe_ilsvrc12.tar.gz

echo "Done."
