#!/usr/bin/env sh
# This scripts downloads the CIFAR10 (binary version) data and unzips it.

DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd "$DIR"

echo "Downloading..."

fname=cifar-10-binary.tar.gz
if [ ! -e $fname  ]; then
    wget --no-check-certificate http://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz
fi

echo "Unzipping..."

#tar -xf cifar-10-binary.tar.gz && rm -f cifar-10-binary.tar.gz
tar -xf cifar-10-binary.tar.gz
mv cifar-10-batches-bin/* . && rm -rf cifar-10-batches-bin

# Creation is split out because leveldb sometimes causes segfault
# and needs to be re-created.

echo "Done."
