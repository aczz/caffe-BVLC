## Caffe

This is Zhuo's fork of Caffe, which fix build(compilation, linking, dependencies) problems. It's not hacking, but **better maintainance**.

**Caffe is the best, really ONNX for model conversion for mobile deployment**!

## Installation & Dependencies

**Linux**
Only tested under ubuntu 16.04

1. install conda & python & pip
- miniconda / anaconda are both OK.
- use python3 since python2 will die from 2020. On default python version set to 3 in CMakeLists.txt
- better management for different python minor versions
- don't need apt install python packages, and also don't recommend
- if `which` pip returns `/usr/local/bin/pip`, do this:
```
sudo pip uninstall pip
conda install pip
```

2. install cmake
- manually install cmake, instead of `apt install cmake` since this only gives cmake 3.5.1, which is too old and buggy.
- 3.14 or later is recomended
- manually install cmake binary from official website, or `conda install cmake`
- cmake were born for **cross-platform make**, however, Caffe's cmake script is not cross-platform. Let's fix it.

3. install boost
- boost if heavy, but caffe use boost. I'll try to replace boost with C++11 gradually
- ubuntu16.04's apt provided boost-python is only for python2. Python2 will retire in 2020. Hence don't use apt to install boost-python if you're using python3.
- just conda install it: `conda install boost` and `conda install py-boost`. For python3.7, it provides boost 1.67
- cmake's finding boost scripts were buggy. Being fixed but may still not work for other not tested versions.
- modify **`cmake/Custom.cmake`** to specifying boost include directory implicitly
- on MacOSX, either brew installed boost or conda installed boost are not OK with Caffe + Python3.

4. HDF5: turned off by default
- I've never been using hdf5 format for both training and inference
- the cmake scripts were wrongly finding HDF5 dependency whatever you turn on or off it. Fixed.

5. leveldb: turned off by default
- thus we don't need snappy on default

6. lmdb: We need it. It's kept for training purpose thou many modern boys only use tf/pytorch/mxnet.
`sudo apt install liblmdb-dev`

7. opencv: we need it. For load image and preprocess.
tested version: manually compiled opencv 4.1.1 / 3.4.5's C/C++ lib, 4.1.1's python lib.

Please compile it like this:
```bash
cmake .. \
    -DCMAKE_INSTALL_PREFIX=/usr/local/opencv-4.1 \
    -DINSTALL_PYTHON_EXAMPLES=ON \
    -DOPENCV_PYTHON3_VERSION=3.7 \
    -DPYTHON3_EXECUTABLE=/home/zz/soft/miniconda3/bin/python \
    -DPYTHON3_INCLUDE_DIR=/home/zz/soft/miniconda3/include/python3.7m \
    -DPYTHON3_LIBRARY=/home/zz/soft/miniconda3/lib/libpython3.7m.so \
    -DBUILD_opencv_python3=ON \
    -DBUILD_opencv_python2=OFF \
    -DPYTHON_DEFAULT_EXECUTABLE=/home/zz/soft/miniconda3/bin/python \
    -DHAVE_opencv_python3=ON \
    -DBUILD_TIFF=ON

make -j8
sudo make install

cd python_loader
python setup.py develop
```

8. cuda/cudnn: we need it for NVidia GPU speedup.

9. protobuf: we need it for model loading and saving.
tested version: `sudo apt install libprotobuf-dev protobuf-compiler`

10. gflags: I'll remove it gradually.

11. glog: I'll remove it gradually. Will be replaced by **spdlog**
Now `include/spdlog` folder using spdlog v1.x code.

12. blas: use OpenBLAS by default.
`sudo apt install libopenblas-dev`

13. Python packages
- numpy: `pip install numpy`
- skimage: `pip install scikit-image`
- jupyter notebook: `conda install jupyter notebook`
- protobuf: must be equal to the version used in cmake (for C/C++). e.g. `pip install protobuf==2.6.1`.

## License and Citation

Caffe is released under the [BSD 2-Clause license](https://github.com/BVLC/caffe/blob/master/LICENSE).
The BAIR/BVLC reference models are released for unrestricted use.

Please cite Caffe in your publications if it helps your research:

    @article{jia2014caffe,
      Author = {Jia, Yangqing and Shelhamer, Evan and Donahue, Jeff and Karayev, Sergey and Long, Jonathan and Girshick, Ross and Guadarrama, Sergio and Darrell, Trevor},
      Journal = {arXiv preprint arXiv:1408.5093},
      Title = {Caffe: Convolutional Architecture for Fast Feature Embedding},
      Year = {2014}
    }
