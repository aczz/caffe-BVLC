#!/usr/bin/env python
#coding: utf-8


import sys
import os
os.environ['GLOG_minloglevel'] = '2'

sys.path.insert(0, '/home/zz/work/caffe-BVLC/python')

import caffe

def check_file(fname):
    if os.path.exists(fname):
        print('%s found.' % fname)
    else:
        raise Exception("Error! %s not exist" % fname)


if __name__ == '__main__':
    caffe.set_device(0)
    caffe.set_mode_gpu()
    #caffe.set_mode_cpu()

    prototxt = '/home/zz/work/convert/PytorchToCaffe/gbodyattr.prototxt'
    caffemodel = '/home/zz/work/convert/PytorchToCaffe/gbodyattr.caffemodel'
    check_file(prototxt)
    check_file(caffemodel)

    net = caffe.Net(prototxt, caffe.TEST)
    net.copy_from(caffemodel)
    net.save('/home/zz/work/convert/PytorchToCaffe/gbodyattr-mod.caffemodel')

