#!/usr/bin/env python
# coding:utf-8

import numpy as np
import logzero

import os
os.environ['GLOG_minloglevel'] = '2'

import sys
caffe_root = '/home/zz/work/caffe-BVLC'
sys.path.insert(0, '/home/zz/work/caffe-BVLC/python')
import caffe

from classify_demo import SDK_Processor, check_file, get_image_lst

class ImageClassify_MobileNet_v1_Processor:
    def __init__(self):
        caffe.set_mode_cpu()

        prototxt = caffe_root+'/models/shicai_mobilenet_v1/mobilenet_deploy.prototxt'
        caffemodel = caffe_root+'/models/shicai_mobilenet_v1/mobilenet.caffemodel'

        self.__net = caffe.Net(str(prototxt), str(caffemodel), caffe.TEST)

        self.__in_h, self.__in_w = 224, 224
        img_mean = np.array([103.94, 116.78, 123.68], dtype=np.float32)

        self.__transformer = caffe.io.Transformer({'data': self.__net.blobs['data'].data.shape})
        self.__transformer.set_transpose('data', (2, 0, 1))  # row to col
        self.__transformer.set_channel_swap('data', (2, 1, 0))  # RGB to BGR
        self.__transformer.set_raw_scale('data', 255)  # [0,1] to [0,255]
        self.__transformer.set_mean('data', img_mean)
        self.__transformer.set_input_scale('data', 0.017)

    def Process(self, im_pth):
        print('!! im_pth is: ', im_pth)
        im = caffe.io.load_image(im_pth)
        h, w, _ = im.shape
        if h < w:
            off = int((w - h) / 2)
            im = im[:, off:off + h]
        else:
            off = (h - w) / 2
            im = im[off:off + h, :]
        im = caffe.io.resize_image(im, [self.__in_h, self.__in_w])

        self.__net.blobs['data'].reshape(1, 3, self.__in_h, self.__in_w)
        self.__net.blobs['data'].data[...] = self.__transformer.preprocess('data', im)
        out = self.__net.forward()
        prob = out['prob']
        prob = np.squeeze(prob)
        idx = np.argsort(-prob)

        label_txt = '/home/zz/work/caffe-BVLC/data/ilsvrc12/synset_words.txt'
        label_names = np.loadtxt(label_txt, str, delimiter='\t')
        for i in range(5):
            label = idx[i]
            print('%.2f - %s' % (prob[label], label_names[label]))
        return

class ImageClassify_MobileNet_v2_Processor:
    def __init__(self):
        caffe.set_mode_cpu()

        prototxt = caffe_root+'/models/shicai_mobilenet_v2/mobilenet_v2_deploy.prototxt'
        caffemodel = caffe_root+'/models/shicai_mobilenet_v2/mobilenet_v2.caffemodel'

        self.__net = caffe.Net(str(prototxt), str(caffemodel), caffe.TEST)

        self.__in_h, self.__in_w = 224, 224
        img_mean = np.array([103.94, 116.78, 123.68], dtype=np.float32)

        self.__transformer = caffe.io.Transformer({'data': self.__net.blobs['data'].data.shape})
        self.__transformer.set_transpose('data', (2, 0, 1))  # row to col
        self.__transformer.set_channel_swap('data', (2, 1, 0))  # RGB to BGR
        self.__transformer.set_raw_scale('data', 255)  # [0,1] to [0,255]
        self.__transformer.set_mean('data', img_mean)
        self.__transformer.set_input_scale('data', 0.017)

    def Process(self, im_pth):
        print('!! im_pth is: ', im_pth)
        im = caffe.io.load_image(im_pth)
        h, w, _ = im.shape
        if h < w:
            off = int((w - h) / 2)
            im = im[:, off:off + h]
        else:
            off = (h - w) / 2
            im = im[off:off + h, :]
        im = caffe.io.resize_image(im, [self.__in_h, self.__in_w])

        self.__net.blobs['data'].reshape(1, 3, self.__in_h, self.__in_w)
        self.__net.blobs['data'].data[...] = self.__transformer.preprocess('data', im)
        out = self.__net.forward()
        prob = out['prob']
        prob = np.squeeze(prob)
        idx = np.argsort(-prob)

        label_txt = '/home/zz/work/caffe-BVLC/data/ilsvrc12/synset_words.txt'
        label_names = np.loadtxt(label_txt, str, delimiter='\t')
        for i in range(5):
            label = idx[i]
            print('%.2f - %s' % (prob[label], label_names[label]))
        return



if __name__ == '__main__':
    processor = ImageClassify_MobileNet_v2_Processor()

    im_lst = get_image_lst()

    for im_pth in im_lst:
        processor.Process(im_pth)
