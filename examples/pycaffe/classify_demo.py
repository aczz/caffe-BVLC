#!/usr/bin/env python
#coding: utf-8

"""
ImageClassify_Processor

The template of SDK in Python implementation
Define a XXX_Processor class, put init stuffs in ctor, uninit in dtor
put core algorithm (preprocess, run cnn, post process) in Process() fun
decomposite ctor into several internal/private functions
"""

from abc import ABC, abstractmethod

import numpy as np
from pprint import pprint
from matplotlib import pyplot as plt

from logzero import setup_logger
import logging

import os
# turn off glog output
os.environ['GLOG_minloglevel'] = '2'

import sys
caffe_root='/home/zz/work/caffe-BVLC'
sys.path.insert(0, '/home/zz/work/caffe-BVLC/python')
import caffe

def check_file(fname):
    if os.path.exists(fname):
        print('%s found.' % fname)
    else:
        raise Exception("Error! %s not exist" % fname)
        #system('../scripts/download_model_binary.py ../models/bvlc_alexnet')


class SDK_Processor(ABC):
    @abstractmethod
    def __init__(self):
        """
        ctor
        """
        pass

    @abstractmethod
    def __del__(self):
        """
        dtor
        """
        pass

    @abstractmethod
    def Process(self):
        """
        core function, perform algorithm(e.g. run cnn)
        """
        pass

    @abstractmethod
    def _save_result(self):
        """
        post-processing, save result, etc
        """
        pass

class ImageClassify_Alexnet_Processor(SDK_Processor):
    def __init__(self):
        """
        ctor of the Processor class
        """
        # --- 0. declare/define member variables
        self.__declare_member_vars()

        # --- 1. setup logging
        self.__setup_logging()

        # --- 2. setup runtime
        self.__setup_runtime()

        # --- 3. setup network
        self.__setup_network()

        # --- 4. setup input(pre-process)
        self.__setup_input()

        # --- 5. setup output(post-process)
        self.__setup_output()

    def Process(self, im_pth):
        """
        load an image from specified path
        and process it using the loaded CNN
        """
        print("processing...")
        image=caffe.io.load_image(im_pth)
        self.__transformed_image = self.__transformer.preprocess('data', image)
        plt.imshow(image)

        self.__image_classify()
        self._save_result()

    def __del__(self):
        """
        dtor of the Processing class
        """
        print("dtor...")

    def __declare_member_vars(self):
        self.__model_def = ''
        self.__model_weights = ''
        self.__transformer = ''
        self.__logger = ''

    def __setup_logging(self):
        logging.basicConfig(level=logging.INFO)
        self.__logger = setup_logger(name='ImageClassify_Alexnet_Processor')

    def __setup_runtime(self):
        #caffe.set_mode_cpu()
        caffe.set_device(0)
        caffe.set_mode_gpu()

    def __setup_network(self):
        prototxt = caffe_root+"/models/bvlc_alexnet/deploy.prototxt"
        caffemodel = caffe_root+"/models/bvlc_alexnet/bvlc_alexnet.caffemodel"
        check_file(prototxt)
        check_file(caffemodel)
        self.__model_def = prototxt
        self.__model_weights = caffemodel

        self.__net = caffe.Net(self.__model_def, self.__model_weights, caffe.TEST)

    def __setup_input(self):
        self.__mu = np.load(caffe_root+'/python/caffe/imagenet/ilsvrc_2012_mean.npy')
        #mu = np.ones_like(mu)
        #self.__logger.info('mean-subtracted values:')
        #zip('BGR', self.__mu)
        #self.__logger.info(list(zip('BGR', self.__mu)))

        self.__transformer = caffe.io.Transformer({'data': self.__net.blobs['data'].data.shape})

        self.__transformer.set_transpose('data', (2,0,1))
        self.__transformer.set_mean('data', self.__mu)
        self.__transformer.set_raw_scale('data', 255)
        self.__transformer.set_channel_swap('data', (2,1,0))

        self.__net.blobs['data'].reshape(1, 3, 227, 227) #N,C,H,W

    def __setup_output(self):
        pass

    def __image_classify(self):
        self.__net.blobs['data'].data[...] = self.__transformed_image

        output = self.__net.forward()
        self.__output_prob = output['prob'][0]

    def _save_result(self):
        output_prob = self.__output_prob
        self.__logger.info('predicted class is: %d', output_prob.argmax())
        # should be 281

        labels_file = caffe_root+'/data/ilsvrc12/synset_words.txt'
        if not os.path.exists(labels_file):
            system("../data/ilsvrc12/get_ilsvrc_aux_fast.sh")

        labels = np.loadtxt(labels_file, str, delimiter='\t')
        self.__logger.info('output label: %s', labels[output_prob.argmax()])

        top_inds = output_prob.argsort()[::-1][:5]

        self.__logger.info('TOP5 probs and labels:')
        res_items = list(zip(output_prob[top_inds], labels[top_inds]))
        for item in res_items:
            prob, label = item
            self.__logger.info('prob: %.4f  label: %s' % (prob, item))

def get_image_lst():
    im_lst = [
        caffe_root+'/examples/images/cat.jpg'
    ]
    return im_lst

if __name__ == '__main__':
    processor = ImageClassify_Alexnet_Processor()

    im_lst = get_image_lst()

    for im_pth in im_lst:
        processor.Process(im_pth)


