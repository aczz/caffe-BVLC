#!/usr/bin/env python
#coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint

plt.rcParams['figure.figsize'] = (10, 10)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

import os
# turn off glog output
os.environ['GLOG_minloglevel'] = '2'

import sys
caffe_root='/home/zz/work/caffe-BVLC'
sys.path.insert(0, '/home/zz/work/caffe-BVLC/python')
import caffe

import os
if os.path.isfile(caffe_root+'/models/bvlc_alexnet/bvlc_alexnet.caffemodel'):
    print('CaffeNet found.')
else:
    print('Downloading pre-trained CaffeNet model...')
    system('../scripts/download_model_binary.py ../models/bvlc_alexnet')

"""
2. Load net and set up input preprocessing
"""
#caffe.set_mode_cpu()
caffe.set_device(0)
caffe.set_mode_gpu()

model_def = caffe_root+"/models/bvlc_alexnet/deploy.prototxt"
model_weights = caffe_root+"/models/bvlc_alexnet/bvlc_alexnet.caffemodel"

net = caffe.Net(model_def,
        model_weights,
        caffe.TEST)

mu = np.load(caffe_root+'/python/caffe/imagenet/ilsvrc_2012_mean.npy')
#mu = np.ones_like(mu)
print('mean-subtracted values:')
print(list(zip('BGR', mu)))

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})

transformer.set_transpose('data', (2,0,1))
transformer.set_mean('data', mu)
transformer.set_raw_scale('data', 255)
transformer.set_channel_swap('data', (2,1,0))

net.blobs['data'].reshape(1, 3, 227, 227) #N,C,H,W

image=caffe.io.load_image(caffe_root+'/examples/images/cat.jpg')
transformed_image = transformer.preprocess('data', image)
plt.imshow(image)

net.blobs['data'].data[...] = transformed_image

output = net.forward()
output_prob = output['prob'][0]

print('predicted class is:', output_prob.argmax())
# should be 281

labels_file = caffe_root+'/data/ilsvrc12/synset_words.txt'
if not os.path.exists(labels_file):
    system("../data/ilsvrc12/get_ilsvrc_aux_fast.sh")

labels = np.loadtxt(labels_file, str, delimiter='\t')
print('output label:', labels[output_prob.argmax()])

top_inds = output_prob.argsort()[::-1][:5]

print('TOP5 probs and labels:')
res_items = list(zip(output_prob[top_inds], labels[top_inds]))
for item in res_items:
    prob, label = item
    print('prob: %.4f  label: %s' % (prob, item))

