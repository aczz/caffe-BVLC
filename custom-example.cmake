#custom cmake's find_package() related stuff, such as OpenCV_DIR cache variable
# please make a copy of this file with new name "custom.cmake" and use it like:
#   mkdir -p build
#   cd build
#   cmake .. -C ../custom.cmake
#   make -j8
#
set(OpenCV_DIR /Users/chris/work/opencv-3.4.5/build/install/share/OpenCV CACHE PATH "")
